**Welcome to our public repository for the addtional content to the Paper we submitted to VaMoS 2020**

**"Using Variability Modeling to Support Security Evaluations: Virtualizing the Right Attack Scenarios"**

The following files are part of the repository in addition to paper

* vulnerability_model/
	* Complete version of the Feature Model representing the threat level for the case study.
	* The model also contains all constraints.
	* The file "model.xml is part of a modelling project we build up in FeatureIDE.
	* The file "image.png" is an image of the model containing all of the constraints.

* Dockerfiles/
	* This folder contains DockerFiles, which descripe the Container image for two of our scenarios
	* S08 - DockerFile - Scenario S08 for Firefox <=42.0, without CVE identifer
	* S10.2 - DockerFile - Scenario S10.2 for Firefox 35.0.1, Flash 11.2.202.466, CVE-2015-3043, CVE-2015-3113 

* Attackscripts/
	* This folder contains the corresponding attack scripts for S08 and S10.2.
	* Each file shows the configuration we used in the Metasploit Framework (MSF).
	* The files can be imported directly into the MSF.
	


